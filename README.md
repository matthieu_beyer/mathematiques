Professeur de mathématiques je vous partage sur cette forge l'ensemble de mes **cours** ainsi que 
mes **activités**, mes **fiches d'exercices** et mes **sujets d'évaluation**.  
Les documents sont rédigés à l'aide de **LaTeX** avec l'éditeur de texte **OverLeaf**. J'utilise essentiellement les packages Profcollege et Proflycee pour rédiger mes documents ainsi que des packages personnels. 
